<?php
/**
* The sidebar containing the second sidebar area.
*
* @package storefront
*/

if ( ! is_active_sidebar( 'blog-sidebar' ) ) {
    return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
    <?php dynamic_sidebar( 'blog-sidebar' ); ?>
</div><!-- #secondary -->