<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06-04-18
 * Time: 14:15
 */

$time = get_the_term_list( $post->ID, 'difficulty' );
$termsDiff = strip_tags( $time );

$diff = get_the_term_list( $post->ID, 'time' );
$termsTime = strip_tags( $diff );
?>

<li>
	<a href="<?php the_permalink(); ?>">
		<div class="image">
			<?php the_post_thumbnail('full'); ?>
		</div>
		<div class="methContent">
			<?php the_title('<h3>','</h3>') ?>
			<span class="intro"><?php echo get_the_excerpt(); ?></span>
			<label class="methodlabel diff <?php echo $termsDiff; ?>"><?php echo $termsDiff; ?></label> 
			<label class="methodlabel time">
				<i class="fa fa-clock-o"></i> <?php echo $termsTime; ?> <?php _e("minutes", "storefront") ?>
			</label>
		</div>
	</a>
</li>
