<?php
/**
 * The template for displaying archive pages.
 *Template Name: Methods
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */

get_header(); ?>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script>
	$(function() {
  setTimeout(function() {
    if (location.hash) {
      /* we need to scroll to the top of the window first, because the browser will always jump to the anchor first before JavaScript is ready, thanks Stack Overflow: http://stackoverflow.com/a/3659116 */
      window.scrollTo(0, 0);
      target = location.hash.split('#');
      smoothScrollTo($('#'+target[1]));
    }
  }, 1);
  
  // taken from: https://css-tricks.com/snippets/jquery/smooth-scrolling/
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      smoothScrollTo($(this.hash));
      return false;
    }
  });
  
  function smoothScrollTo(target) {
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    
    if (target.length) {
      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
    }
  }
});
</script>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
			<h1><?php _e("Methods", "storefront") ?></h1>
			<p><?php _e("The Brain Fuel methods are specifically selected and developed to help you deploy the cards in a manner that will lead you to the best ideas. With <strong>various levels of difficulty, duration and needed creativity</strong> we have methods for every kind of brainstorming situation. Are you a brainstorming rookie, and looking for a method for quick ideas? Check out the 1-3 stars difficulty methods. Are you an experienced brainstormer and looking for a real gamechanger? Try out a more difficult one, like Brain Fusion or the HIT!", "storefront") ?></p>

<ul class="methodMenu">
	<?php // Output all Taxonomies names with their respective items
		$terms = get_terms('phase');
		foreach( $terms as $term ):
	?>
		<li><a href="#<?php echo $term->slug; ?>"><?php echo $term->name; // Print the term name ?></a></li>  
	<?php endforeach; ?>           
</ul>

			<?php // Output all Taxonomies names with their respective items
$terms = get_terms('phase');
foreach( $terms as $term ):
?>                          
    <h4 id="<?php echo $term->slug; ?>"><?php echo $term->name;  // Print the term name ?> <?php _e("methods", "storefront") ?></h4>                          
    <ul class="methodesList">
      <?php                         
          $posts = get_posts(array(
            'post_type' => 'methods',
            'taxonomy' => $term->taxonomy,
            'term' => $term->slug,  
            'orderby'  => 'title',
            'order'     => 'ASC',                                
            'nopaging' => true, // to show all posts in this taxonomy, could also use 'numberposts' => -1 instead
          ));
          foreach($posts as $post): // begin cycle through posts of this taxonmy
            setup_postdata($post); //set up post data for use in the loop (enables the_title(), etc without specifying a post ID)
      
      		get_template_part('item-methods');
       endforeach; ?>
    </ul>                                                   
<?php endforeach; ?>

	</main>
</div>
<?php
get_footer();
