<?php
/**
 * The template for displaying archive pages.
 *Template Name: Methods
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront
 */

get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
			<h1><?php _e("Our facilitators", "storefront") ?></h1>
			<p><?php _e("Hiring a certified Brain Fuel Facilitator allows you to bring your session to the next level. All of our certified facilitators have been trained to use Brain Fuel to guide your session and the participants to maximize efficiency and output.</br><br/><a href='tel:0031681449925'>Give us a call</a>, and we'll help you find the right facilitator for the job!<br/><br/>Would you like to become a certified Brain Fuel Facilitator yourself? Then join us for one of our two-day training sessions.<br /><br/><a class='btn' href='https://www.brainfuel.nl/en/academy/brain-fuel-facilitator-training/'>Sign up for our Facilitator Training</a> <a class='btn' href='https://www.brainfuel.nl/en/academy/brain-fuel-facilitator-training/'>Give us a call</a>", "storefront") ?></p>

			<?php if ( have_posts() ) : 
				echo '<div class="container">
				<div class="wide">
					<ul class="facilitatorList">';
					while ( have_posts() ) : the_post(); 

				 		get_template_part( '/template-parts/item-facilitator' );
					endwhile;

				echo '</ul></div></div>';

		else :

			get_template_part( 'content', 'none' );

		endif; ?>
		</main>
</div>
<?php
get_footer();
