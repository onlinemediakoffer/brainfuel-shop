<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

get_header(); 

$diff = get_the_term_list( $post->ID, 'difficulty' );
$termsDiff = strip_tags( $diff );

$time = get_the_term_list( $post->ID, 'time' );
$termsTime = strip_tags( $time );

$phasss = get_the_term_list( $post->ID, 'phase' );
$termsPhase = strip_tags( $phasss );


$cards = get_the_term_list( $post->ID, 'card', '<label class="methodlabel cards">', ', ', '</label>' );
$termsCards = strip_tags( $cards );
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post();

			do_action( 'storefront_single_post_before' ); ?>
			<div class="methDetailContainer">
				<div class="left">
					<?php the_post_thumbnail('full'); ?>
				</div>
				<div class="right">
					<?php the_title('<h1>','</h1>') ?>
					<span class="intro"><?php echo get_the_excerpt(); ?></span>
					<br/>
					<br/>
					<table class="methodTable" cellpadding="0" cellspacing="0">
						<tr><td>
								<label class="specs"><?php _e("Phase", "storefront") ?></label>
							</td><td>
								<label class="phase"><?php echo $termsPhase; ?></label>
							</td>
						</tr><tr><td>
								<label class="specs"><?php _e("Difficulty", "storefront") ?></label>
							</td><td>
								<label class="methodlabel diff <?php echo $termsDiff; ?>"><?php echo $termsDiff; ?></label>
							</td>
						</tr><tr><td>
								<label class="specs"><?php _e("Time needed", "storefront") ?></label>
							</td><td>
								<label class="methodlabel time"><i class="fa fa-clock-o"></i> <?php echo $termsTime; ?> <?php _e("minutes", "storefront") ?></label>
							</td>
						</tr><tr>
							<td>
								<label class="specs"><?php _e("Cards needed", "storefront") ?></label>
							</td><td>
								<?php echo $termsCards; ?>
							</td>
						</tr><tr><td>
								<label class="specs"><?php _e("Tags:", "storefront") ?></label>
							</td><td>
								 <?php $posttags = get_the_tags(); 
								 if ($posttags) {
								 	foreach($posttags as $tag) {
								 		echo '<label class="methodlabel tags">' .$tag->name. '</label>'; 
								 	}
								 }
								 ?>
							</td>
						</tr></table>
				</div>
			</div>


			<?php get_template_part( 'content', 'single' );

			do_action( 'storefront_single_post_after' );

		endwhile; // End of the loop. ?>

		</main><!-- #main -->
	<!-- #primary -->

<div id="secondary">
<?php dynamic_sidebar('sidebar-1'); ?>
</div>
<?php get_footer();
