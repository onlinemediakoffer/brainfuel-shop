/**
 * Created by Richard on 16-04-18.
 */

var containerEl = document.querySelector('.methodesList');

var mixer = mixitup(containerEl, {
    multifilter: {
        enable: true
    },
    animation: {
        effects: 'fade translateZ(-100px)'
    }
});