<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}


// function brainfuel_methods_filter_js() {
//     wp_enqueue_script( 'mixitup', get_stylesheet_directory_uri() . '/js/mixitup.min.js', array(), '1.0.0', true );
//     wp_enqueue_script( 'mixitup-multifilter', get_stylesheet_directory_uri() . '/js/mixitup-multifilter.min.js', array(), '1.0.0', true );
//     wp_enqueue_script( 'mixitup-options', get_stylesheet_directory_uri() . '/js/bf_methods_filter.js', array(), '1.0.0', true );
// }
// add_action( 'wp_enqueue_scripts', 'brainfuel_methods_filter_js' );

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

/******************************************************************
 * Adds a custom menu
******************************************************************/
function custom_topbar_menu() {
  register_nav_menu('topbar-menu',__( 'Topbar menu' ));
  register_nav_menu('disclaimer-menu',__( 'Disclaimer menu' ));
}
add_action( 'init', 'custom_topbar_menu' );

/******************************************************************
 * Extra blok on homepage
 ******************************************************************/
function topcontent_homepage() {
    if ( is_front_page() ) { 
   
    get_template_part( 'template-parts/homepage', 'header' );

    }

}
add_action( 'storefront_before_content', 'topcontent_homepage' );

/******************************************************************
 * More item on homepage Storefront
******************************************************************/

add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 40;
  return $cols;
}

/******************************************************************
 * Remove searchbar from header
 ******************************************************************/
function remove_sf_actions() {

	remove_action( 'storefront_header', 'storefront_product_search', 40 );

}
add_action( 'init', 'remove_sf_actions' );

/******************************************************************
 * Add call to action before footer
******************************************************************/
function add_cta_before_footer() {
    
    get_template_part( 'template-parts/footer', 'cta' );
}
add_action( 'storefront_before_footer', 'add_cta_before_footer' );


/******************************************************************
 * Replace logo
 ******************************************************************/
add_action( 'init', 'storefront_custom_logo' );
function storefront_custom_logo() {
    remove_action( 'storefront_header', 'storefront_site_branding', 20 );
    add_action( 'storefront_header', 'storefront_display_custom_logo', 20 );
}

function storefront_display_custom_logo() {
?>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo-link" rel="home">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Logo-brain-fuel-zonder-payoff.svg" alt="<?php echo get_bloginfo( 'name' ); ?>" />
    </a>
<?php
}

/******************************************************************
 * Add disclaimer menu and store icons to bottom of the footer
 ******************************************************************/
function add_menu_after_footer() {
    ?>
    <div class="bottomMenu">
        <!-- <div class="storeIcons">
            <a href="https://itunes.apple.com/nl/app/brain-fuel/id1178990987" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/itunes-badge.svg"/></a>
            <a href="https://play.google.com/store/apps/details?id=nl.onlinemediakoffer.brainfuel" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/google-play-badge.svg"/></a>
        </div> -->
        <?php
            wp_nav_menu( array( 
            'theme_location' => 'disclaimer-menu', 
            'container_class' => 'disclaimer-menu' ) ); 
        ?>
        <div class="credits">© BRAIN FUEL BV <span class="register">®</span> - <?php echo date("Y"); ?></div>
    </div>
    <?php

}
add_action( 'storefront_footer', 'add_menu_after_footer', 20 );

/******************************************************************
* Remove "Storefront Designed by WooThemes" from Footer
******************************************************************/
add_action( 'init', 'custom_remove_footer_credit', 10 );
function custom_remove_footer_credit () {
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
} 


/******************************************************************
* Add extra button in menu block
******************************************************************/
add_action( 'storefront_header', 'custom_cta_menu_item', 70 );
function custom_cta_menu_item () {
    ?>
    <a class="shiftnav-toggle shiftnav-toggle-button" data-shiftnav-target="shiftnav-main"><i class="fa fa-bars"></i> Menu </a>
    <a href="<?php echo get_site_url(); ?><?php _e("/en/webshop/", "storefront") ?>" class="btn headerCta one">Webshop</a> 
    <a href="<?php echo get_site_url(); ?><?php _e("/en/webshop/", "storefront") ?>" class="btn headerCta two"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>     
     <?php
} 

/******************************************************************
 * Add sidebar
 ******************************************************************/
function wpb_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Topbar',
        'id'            => 'custom-header-widget',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="chw-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar(array(
        'id'            => 'blog-sidebar',
        'name'          => __( 'blog sidebar', 'storefront' ),
        'description'   => __( 'blog sidebar.', 'storefront' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Submenu', 'storefront' ),
        'id'            => 'submenu',
        'description'   => esc_html__( 'Add widgets here.', 'storefront' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
 
}
add_action( 'widgets_init', 'wpb_widgets_init' );

/******************************************************************
 * Adds a top bar to Storefront, before the header.
 ******************************************************************/
function storefront_add_topbar() {
    ?>
    <div class="topbar">
        <div class="topbarInner">
            <div class="col-full">
                <?php
    			wp_nav_menu( array( 
    			    'theme_location' => 'topbar-menu', 
    			    'container_class' => 'topbar-menu' ) ); 
    			?>
            </div>
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Topbar") ) : ?>
    		<?php endif;?>
        </div>
    </div>
    <?php
}
add_action( 'storefront_before_site', 'storefront_add_topbar' );


/******************************************************************
 * Remove homepage hooks
 ******************************************************************/
function remove_homepage_hooks() {
    //remove_action( 'homepage', 'storefront_homepage_content', 10 );
    remove_action( 'homepage', 'storefront_product_categories', 20 );
    remove_action( 'homepage', 'storefront_recent_products', 30 );
    remove_action( 'homepage', 'storefront_featured_products', 40 );
    remove_action( 'homepage', 'storefront_popular_products', 50 );
    remove_action( 'homepage', 'storefront_on_sale_products', 60 );
    remove_action( 'homepage', 'storefront_best_selling_products', 70 );
}
add_action( 'init', 'remove_homepage_hooks' );

/**      * Remove existing tabs from single product pages.       */     
function remove_woocommerce_product_tabs( $tabs ) {
         unset( $tabs['description'] );          
         unset( $tabs['reviews'] );          
         unset( $tabs['additional_information'] );           
         return $tabs;       
}       

add_filter( 'woocommerce_product_tabs', 'remove_woocommerce_product_tabs', 98 );
/**      * Hook in each tabs callback function after single content.         */            
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_product_description_tab' );
// add_action( 'woocommerce_after_single_product_summary', 'woocommerce_product_additional_information_tab' );
//add_action( 'woocommerce_after_single_product_summary', 'comments_template' );




function storefront_post_header() {
        ?>
        <header class="entry-header">
        <?php
        if ( is_single() ) {     
            the_title( '<h1 class="entry-title">', '</h1>' );
            storefront_posted_on();
            $categories_list = get_the_category_list( __( ', ', 'storefront' ) );

            if ( $categories_list ) : ?>
                <div class="cat-links">
                    <?php
                    echo '<div class="label">' . esc_attr( __( 'Posted in', 'storefront' ) ) . '</div>';
                    echo wp_kses_post( $categories_list );
                    ?>
                </div>
            <?php endif; // End if categories. 
                    ?>
                </div><?php
        } else {
           

            the_title( sprintf( '<h2 class="alpha entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

             if ( 'post' == get_post_type() ) {
                storefront_posted_on();
                $categories_list = get_the_category_list( __( ', ', 'storefront' ) );

            if ( $categories_list ) : ?>
                <div class="cat-links">
                    <?php
                    echo '<div class="label">' . esc_attr( __( 'Posted in', 'storefront' ) ) . '</div>';
                    echo wp_kses_post( $categories_list );
                    ?>
                </div>
                <?php endif; // End if categories. 
                    
            }
        }
        ?>
        </header><!-- .entry-header -->
        <?php
    }

/// Excerpt toevoegen op blog archives

add_action( 'init', 'jk_customise_storefront' );

function jk_customise_storefront() {
    // Remove the storefromt post content function
    remove_action( 'storefront_loop_post', 'storefront_post_content', 30 );

    // Add our own custom function
    add_action( 'storefront_loop_post', 'jk_custom_storefront_post_content', 30 );
}

function jk_custom_storefront_post_content() {
    ?>
    <div class="entry-content" itemprop="articleBody">
        <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'full', array( 'itemprop' => 'image' ) );
            }
        ?>
        <?php the_excerpt(); ?>
        <p class="read-more"><a class="button" href="<?php the_permalink(); ?>"><?php _e("Read more", "storefront") ?></a></p>
    </div><!-- .entry-content -->
    <?php
}



add_filter('mod_rewrite_rules', 'fix_rewritebase');
function fix_rewritebase($rules){
    $home_root = parse_url(home_url());
    if ( isset( $home_root['path'] ) ) {
        $home_root = trailingslashit($home_root['path']);
    } else {
        $home_root = '/';
    }
 
    $wpml_root = parse_url(get_option('home'));
    if ( isset( $wpml_root['path'] ) ) {
        $wpml_root = trailingslashit($wpml_root['path']);
    } else {
        $wpml_root = '/';
    }
 
    $rules = str_replace("RewriteBase $home_root", "RewriteBase $wpml_root", $rules);
    $rules = str_replace("RewriteRule . $home_root", "RewriteRule . $wpml_root", $rules);
 
    return $rules;
}


/**
 * Show sale prices in the cart.
 */
function my_custom_show_sale_price_at_cart( $old_display, $cart_item, $cart_item_key ) {
    /** @var WC_Product $product */
    $product = $cart_item['data'];
    if ( $product ) {
        return $product->get_price_html();
    }
    return $old_display;
}
add_filter( 'woocommerce_cart_item_price', 'my_custom_show_sale_price_at_cart', 10, 3 );
 
/**
 * Show sale prices at the checkout.
 */
function my_custom_show_sale_price_at_checkout( $subtotal, $cart_item, $cart_item_key ) {
    /** @var WC_Product $product */
    $product = $cart_item['data'];
    $quantity = $cart_item['quantity'];
    if ( ! $product ) {
        return $subtotal;
    }
    $regular_price = $sale_price = $suffix = '';
    if ( $product->is_taxable() ) {
        if ( 'excl' === WC()->cart->tax_display_cart ) {
            $regular_price = wc_get_price_excluding_tax( $product, array( 'price' => $product->get_regular_price(), 'qty' => $quantity ) );
            $sale_price    = wc_get_price_excluding_tax( $product, array( 'price' => $product->get_sale_price(), 'qty' => $quantity ) );
            if ( WC()->cart->prices_include_tax && WC()->cart->tax_total > 0 ) {
                $suffix .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
            }
        } else {
            $regular_price = wc_get_price_including_tax( $product, array( 'price' => $product->get_regular_price(), 'qty' => $quantity ) );
            $sale_price = wc_get_price_including_tax( $product, array( 'price' => $product->get_sale_price(), 'qty' => $quantity ) );
            if ( ! WC()->cart->prices_include_tax && WC()->cart->tax_total > 0 ) {
                $suffix .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
            }
        }
    } else {
        $regular_price    = $product->get_price() * $quantity;
        $sale_price       = $product->get_sale_price() * $quantity;
    }
    if ( $product->is_on_sale() && ! empty( $sale_price ) ) {
        $price = wc_format_sale_price(
                     wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price(), 'qty' => $quantity ) ),
                     wc_get_price_to_display( $product, array( 'qty' => $quantity ) )
                 ) . $product->get_price_suffix();
    } else {
        $price = wc_price( $regular_price ) . $product->get_price_suffix();
    }
    // VAT suffix
    $price = $price . $suffix;
    return $price;
}
add_filter( 'woocommerce_cart_item_subtotal', 'my_custom_show_sale_price_at_checkout', 10, 3 );



/**
 * Show savings at the cart.
 */
function my_custom_buy_now_save_x_cart() {
    $savings = 0;
    foreach ( WC()->cart->get_cart() as $key => $cart_item ) {
        /** @var WC_Product $product */
        $product = $cart_item['data'];
        if ( $product->is_on_sale() ) {
            $savings += ( $product->get_regular_price() - $product->get_sale_price() ) * $cart_item['quantity'];
        }
    }
    if ( ! empty( $savings ) ) {
        ?><tr class="order-savings">
            <th><?php _e('Your savings', 'storefront') ?></th>
            <td data-title="<?php _e('Your savings', 'storefront') ?>"><?php echo sprintf( __( 'Buy now and save %s!', 'storefront'), wc_price( $savings ) ); ?></td>
        </tr><?php
    }
}
add_action( 'woocommerce_cart_totals_before_order_total', 'my_custom_buy_now_save_x_cart' );


/**
 * Coll-full einde verplaatsen naar het einde
 */
add_action( 'init', 'change_order_header_container', 10 );
function change_order_header_container () {
    remove_action( 'storefront_header', 'storefront_header_container_close', 41 );
    add_action( 'storefront_header', 'storefront_header_container_close', 90 );
} 
