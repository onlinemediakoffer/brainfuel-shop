<?php
/**
 * Template part to show the CTA in the footer
 *
 * @package storefront
 */
?>
<div class="col-full homepageTopBlock">
    <div class="homepageTopBlock-middle">
      <div class="headerTitelBlock">
        <h1><?php _e("Ignite your ideation!", "storefront") ?></h1>
        <p><?php _e("Brain Fuel is the fastest, easiest and funniest method to help you solve problems and find opportunities.", "storefront") ?></p>
      </div>
        <div class="homepageTopBlock-button">
            <ul class="menu-parent">
                <li class="sub-menu-parent" tab-index="0">
                   <button class="btn"><?php _e("I need more creativity", "storefront") ?></button>
                   <ul class="sub-menu">
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/cards-methods/", "storefront") ?>"><?php _e("Tools & methods", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/articles/", "storefront") ?>"><?php _e("Articles", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/academy/brain-fuel-toolkit-training/", "storefront") ?>"><?php _e("Get trained in Brain Fuel", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/academy/brain-fuel-facilitator-training/", "storefront") ?>"><?php _e("Become a facilitator", "storefront") ?></a></li>
                   </ul>
                 </li>
                 <li class="sub-menu-parent right" tab-index="0">
                   <button class="button"><?php _e("My company needs creativity", "storefront") ?></button>
                   <ul class="sub-menu">
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/academy/brain-fuel-masterclass/", "storefront") ?>"><?php _e("Masterclasses", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/sessions/", "storefront") ?>"><?php _e("Brainstorms & hackathons", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/academy/", "storefront") ?>"><?php _e("Train your team", "storefront") ?></a></li>
                     <li><a href="<?php echo get_site_url(); ?><?php _e("/en/academy/", "storefront") ?>"><?php _e("Train your company", "storefront") ?></a></li>
                   </ul>
                 </li>
             </ul>
        </div>
        <div class="homepageTopBlock-video">
            <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/314475712?title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
        </div>
    </div>
</div>