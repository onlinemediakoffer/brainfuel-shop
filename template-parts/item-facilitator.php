<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06-04-18
 * Time: 14:15
 */

?>

<li class="mix <?php echo $filters ?>">
	<a href="<?php the_permalink(); ?>">
		<div class="image">
			<?php the_post_thumbnail('full'); ?>
		</div>
		<div class="content">
			<?php the_title('<h2>','</h2>') ?>
			<span class="intro"><?php echo get_the_excerpt(); ?></span>
			<span class="textbtn"><?php _e("Read more about", "storefront")?> <?php the_field('field_5c9386f10deba'); ?></span>
		</div>
	</a>
</li>
