<?php
/**
 * Template part to show the CTA in the footer
 *
 * @package storefront
 */
?>



<div class="ctaBlock 3">
<?php

if( have_rows('call_to_action_instellen') ):
    while( have_rows('call_to_action_instellen') ): the_row();

    $bf_cta_true = get_sub_field('call_to_action_overschrijven');
    $bf_cta_title = get_sub_field('call_to_action_titel');
    $bf_cta_subtitle = get_sub_field('call_to_action_subtitel');
    $bf_cta_link = get_sub_field('call_to_action_link');

    if( $bf_cta_link ): 
        $link_url = $bf_cta_link['url'];
        $link_title = $bf_cta_link['title'];
        $link_target = $bf_cta_link['target'] ? $bf_cta_link['target'] : '_self';
        
    endif; 

    endwhile; 
endif;

	if ( $bf_cta_true ==1){
        
        echo '<h2>'.$bf_cta_title.'</h2>';
        echo '<p>'.$bf_cta_subtitle.'</p>';
        ?>
        <div class="backButton">
            <a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>
        <?php
    }
    else{
        ?>
        <h2><?php _e("Ignite your ideation", "storefront") ?></h2>
        <p><?php _e("Buy Brain Fuel in our store now!", "storefront") ?>
        <div class="backButton">
            <a href="<?php echo get_site_url(); ?><?php _e("/en/webshop/products/brain-fuel-english-deck-of-cards/", "storefront") ?>" class="btn"><?php _e("Buy Brain Fuel", "storefront") ?></a>
        </div>
        </p>
        <?php
    }
?>
</div>